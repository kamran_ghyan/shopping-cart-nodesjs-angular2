var express = require('express');
var router = express.Router();

// Mongo Connection
var mongojs = require('mongojs');
var db = mongojs('mongodb://kamran.g:kamran.g@ds149278.mlab.com:49278/onlineshop', ['users', 'products', 'categories', 'orders']);

// Get All categories
router.get('/categories', function(req, res, next){
    db.categories.find(function(err, categories){
        if(err){
            res.send(err);
        }
        res.json(categories);
    });
});

// Get Single category
router.get('/category/:id', function(req, res, next){
    db.categories.findOne({_id: mongojs.ObjectId(req.params.id)}, function(err, category){
        if(err){
            res.send(err);
        }
        res.json(category);
    });
});

// Save category
router.post('/category', function(req, res, next){
   // Posted data save into variable
   var category = req.body;

   // Posted data validation
   if(!category.title){
       res.status(400);
       res.json({
           "error": "Bad Data"
       });
   } else {
       db.categories.save(category, function(err, category){
        if(err){
            res.send(err);
        }
        res.json(category);
       });
   }
});

// Delete category
router.delete('/category/:id', function(req, res, next){
    db.categories.remove({_id: mongojs.ObjectId(req.params.id)}, function(err, category){
        if(err){
            res.send(err);
        }
        res.json(category);
    });
});

// Update category
router.put('/category/:id', function(req, res, next){
    // Posted data save into variable
    var category = req.body;
    var updateTask = {};

    if(task.title && task.body && task.imageUrl && task.price){
        updateTask.title = task.title;
        updateTask.body = task.body;
        updateTask.imageUrl = task.imageUrl;
        updateTask.price = task.price;
    }

    if(!updateTask){
        res.status(400);
        res.json({
            "error":"Bad Data"
        });
    } else {
        db.products.remove({_id: mongojs.ObjectId(req.params.id)}, updateTask, {}, function(err, product){
            if(err){
                res.send(err);
            }
            res.json(product);
        });
    }

    
});


// Export route module
module.exports = router;