var express = require('express');
var router = express.Router();

// Create Router
router.get('/', function(req, res, next){
    res.render('index.html');
});

// Export route module
module.exports = router;