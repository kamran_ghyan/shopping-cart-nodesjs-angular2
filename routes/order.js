var express = require('express');
var router = express.Router();

// Mongo Connection
var mongojs = require('mongojs');
var db = mongojs('mongodb://kamran.g:kamran.g@ds149278.mlab.com:49278/onlineshop', ['users', 'products', 'categories', 'orders']);

// Save Task
router.post('/order', function(req, res, next){

   // Posted data save into variable
   var order = req.body;
   console.log(order);
   // Posted data validation
   /*if(!order){
       res.status(400);
       res.json({
           "error": "Bad Data"
       });
   } else {
       db.orders.save(order, function(err, order){
        if(err){
            res.send(err);
        }
        res.json(order);
       });
}*/
});

// Delete Product
router.delete('/order/:id', function(req, res, next){
    db.orders.remove({_id: mongojs.ObjectId(req.params.id)}, function(err, order){
        if(err){
            res.send(err);
        }
        res.json(order);
    });
});

// Export route module
module.exports = router;