var express = require('express');
var router = express.Router();

// Mongo Connection
var mongojs = require('mongojs');
var db = mongojs('mongodb://kamran.g:kamran.g@ds149278.mlab.com:49278/onlineshop', ['users', 'products', 'categories', 'orders']);

// Get All Products
router.get('/products', function(req, res, next){
    db.products.find(function(err, products){
        if(err){
            res.send(err);
        }
        res.json(products);
    });
});

// Get Single Product
router.get('/product/:id', function(req, res, next){
    db.products.findOne({_id: mongojs.ObjectId(req.params.id)}, function(err, product){
        if(err){
            res.send(err);
        }
        res.json(product);
    });
});

// Save Task
router.post('/product', function(req, res, next){
   // Posted data save into variable
   var product = req.body;

   // Posted data validation
   if(!product.title){
       res.status(400);
       res.json({
           "error": "Bad Data"
       });
   } else {
       db.products.save(product, function(err, product){
        if(err){
            res.send(err);
        }
        res.json(product);
       });
   }
});

// Delete Product
router.delete('/product/:id', function(req, res, next){
    db.products.remove({_id: mongojs.ObjectId(req.params.id)}, function(err, product){
        if(err){
            res.send(err);
        }
        res.json(product);
    });
});

// Update Product
router.put('/product/:id', function(req, res, next){
    // Posted data save into variable
    var product = req.body;
    var updateTask = {};

    if(task.title && task.body && task.imageUrl && task.price){
        updateTask.title = task.title;
        updateTask.body = task.body;
        updateTask.imageUrl = task.imageUrl;
        updateTask.price = task.price;
    }

    if(!updateTask){
        res.status(400);
        res.json({
            "error":"Bad Data"
        });
    } else {
        db.products.remove({_id: mongojs.ObjectId(req.params.id)}, updateTask, {}, function(err, product){
            if(err){
                res.send(err);
            }
            res.json(product);
        });
    }

    
});


// Export route module
module.exports = router;