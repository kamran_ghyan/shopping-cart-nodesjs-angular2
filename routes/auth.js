var express = require('express');
var jwt = require('jwt-simple');
var router = express.Router();

// Mongo Connection
var mongojs = require('mongojs');
var db = mongojs('mongodb://kamran.g:kamran.g@ds149278.mlab.com:49278/onlineshop', ['users', 'products', 'categories', 'orders']);

    //AUTHENTICATION
    // login a user and create a token
    router.post('/signin', function(req, res, next){
        db.users.findOne({
             email: req.body.email,
             password: req.body.password
        }, function(err, user){

            // Generate token
            var token = Math.random().toString(36).substring(7);

           // Validate
           if(!user){
                res.json({success: false, msg: 'Signin failed, User not found.'});
            } else {    
                    res.json({success: true, token: token});
            }

        });
    });

     // register a user
    router.post('/signup', function(req, res, next){

       var user = req.body;

       if (!req.body.email || !req.body.password) {
            res.json({success: false, msg: 'Please pass email and password.'});
        } else {

            // CHECK EMAIL
            // isEmail
            if(!validator.isEmail(req.body.email)){
                return res.json({success: false, msg: 'Your email is not valid.'});
            }

            // CHECK PASSWORD
            // length
            if(!validator.isLength(req.body.password, {min:5, max: 15})){
                return res.json({success: false, msg: 'Your password must be between 6 and 16 characters.'});
            }
            
            // alphanumeric
            if(!validator.isAlphanumeric(req.body.password)){
                return res.json({success: false, msg: 'Your password must contain only letters and numbers.'});
            }

            db.users.save(user, function(err, product){
            if(err){
                res.send(err);
            }
            res.json(product);
        });
            }
    });


// Export route module
module.exports = router;