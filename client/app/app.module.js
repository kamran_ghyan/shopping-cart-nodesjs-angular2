"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var app_component_1 = require("./app.component");
var products_component_1 = require("./components/products/products.component");
var product_single_component_1 = require("./components/products/product-single.component");
var contact_component_1 = require("./components/pages/contact.component");
var cart_component_1 = require("./components/cart/cart.component");
var checkout_component_1 = require("./components/checkout/checkout.component");
var signin_component_1 = require("./components/auth/signin.component");
var signup_component_1 = require("./components/auth/signup.component");
var header_component_1 = require("./components/layout/header/header.component");
var footer_component_1 = require("./components/layout/footer/footer.component");
var categories_component_1 = require("./components/categories/categories.component");
var nav_component_1 = require("./components/nav/nav.component");
var filter_pipe_1 = require("./utils/filter.pipe");
var routes = [
    { path: '', component: products_component_1.ProductsComponent },
    { path: 'products', component: products_component_1.ProductsComponent },
    { path: 'product/:id', component: product_single_component_1.ProductSingleComponent },
    { path: 'contact', component: contact_component_1.ContactComponent },
    { path: 'cart', component: cart_component_1.CartComponent },
    { path: 'signin', component: signin_component_1.SignInComponent },
    { path: 'signup', component: signup_component_1.SignUpComponent },
    { path: 'checkout', component: checkout_component_1.CheckoutComponent },
    { path: 'categories', component: categories_component_1.CategoriesComponent }
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, http_1.HttpModule, forms_1.FormsModule, router_1.RouterModule.forRoot(routes)],
        declarations: [
            app_component_1.AppComponent,
            products_component_1.ProductsComponent,
            product_single_component_1.ProductSingleComponent,
            contact_component_1.ContactComponent,
            cart_component_1.CartComponent,
            checkout_component_1.CheckoutComponent,
            categories_component_1.CategoriesComponent,
            nav_component_1.NavComponent,
            signin_component_1.SignInComponent,
            signup_component_1.SignUpComponent,
            header_component_1.HeaderComponent,
            filter_pipe_1.FilterItemsPipe,
            footer_component_1.FooterComponent
        ],
        bootstrap: [app_component_1.AppComponent]
    }),
    __metadata("design:paramtypes", [])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map