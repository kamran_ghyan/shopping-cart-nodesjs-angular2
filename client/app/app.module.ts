// GLOABAL IMPORTS
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// LOCAL IMPORTS
import { AppComponent }  from './app.component';
import { ProductsComponent }  from './components/products/products.component';
import { ProductSingleComponent }  from './components/products/product-single.component';
import { ContactComponent }  from './components/pages/contact.component';
import { CartComponent }  from './components/cart/cart.component';
import { CheckoutComponent }  from './components/checkout/checkout.component';
import { SignInComponent }  from './components/auth/signin.component';
import { SignUpComponent }  from './components/auth/signup.component';
import { HeaderComponent }  from './components/layout/header/header.component';
import { FooterComponent }  from './components/layout/footer/footer.component';
import { CategoriesComponent }  from './components/categories/categories.component';
import { NavComponent }  from './components/nav/nav.component';
import {FilterItemsPipe} from './utils/filter.pipe';


const routes: Routes = [
  { path: '', component: ProductsComponent },
  { path: 'products', component: ProductsComponent },
  { path: 'product/:id', component: ProductSingleComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'cart', component: CartComponent },
  { path: 'signin', component: SignInComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'checkout', component: CheckoutComponent },
  { path: 'categories', component: CategoriesComponent }
];

@NgModule({
  imports:      [ BrowserModule, HttpModule, FormsModule, ReactiveFormsModule, RouterModule.forRoot(routes) ],
  declarations: [ 
    AppComponent, 
    ProductsComponent, 
    ProductSingleComponent, 
    ContactComponent, 
    CartComponent, 
    CheckoutComponent, 
    CategoriesComponent, 
    NavComponent, 
    SignInComponent,
    SignUpComponent,
    HeaderComponent, 
    FilterItemsPipe,
    FooterComponent 
    ],
  bootstrap:    [ AppComponent ]
})



export class AppModule {

   
    
 }
