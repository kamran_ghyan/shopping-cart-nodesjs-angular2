// GLOABAL IMPORTS
import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// SERVICES IMPORTS
import { ProductService } from './services/products/products.service';
import { CategoryService } from './services/category/category.service';
import { AuthService } from './services/auth/auth.service';
import { CartService } from './services/cart/cart.service';
import { OrderService } from './services/order/order.service';


@Component({
  selector: 'my-app',
  template: `
  <header></header>
    <section class="main">
      <div class="container">
        <router-outlet></router-outlet>
      </div>
    <section>
  <footer></footer>
  `,
  providers: [ProductService, CategoryService, CartService, AuthService, OrderService]
})
export class AppComponent {  }
