"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var CartService = (function () {
    function CartService() {
        this.cart = [];
    }
    // Add item to cart
    CartService.prototype.addItem = function (item) {
        this.cart.push(item);
    };
    // Delete item to cart
    CartService.prototype.deleteItem = function (item) {
        this.cart = this.cart.filter(function (cartItem) { return cartItem._id !== item._id; });
    };
    // Empty cart
    CartService.prototype.clearCart = function () {
        this.cart = [];
    };
    // Get list of item
    CartService.prototype.getCart = function () {
        return this.cart;
    };
    // Get list of item
    CartService.prototype.getTotalItems = function () {
        return 'test';
    };
    // Get total price
    CartService.prototype.getTotalPrice = function () {
        var totalPrice = this.cart.reduce(function (sum, cartItem) {
            return sum += cartItem.price, sum;
        }, 0);
        return totalPrice;
    };
    return CartService;
}());
CartService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], CartService);
exports.CartService = CartService;
//# sourceMappingURL=cart.service.js.map