// GLOBAL IMPORT
import {Injectable} from '@angular/core';

// GLOBAL MODEL
import { Product } from '../../../model/product/product';


@Injectable()

export class CartService{

    private cart:Product[]=[];

    // Add item to cart
    addItem(item:Product){
        this.cart.push(item);
    }
    // Delete item to cart
    deleteItem(item:Product){
        this.cart = this.cart.filter(cartItem=>cartItem._id!==item._id);
    }
    // Empty cart
    clearCart(){
        this.cart = [];
    }
    
   // Get list of item
    getCart():Product[]{
        return this.cart;
    }
     
    // Get total price
    getTotalPrice(){
        let totalPrice = this.cart.reduce((sum, cartItem)=>{
            return sum+=cartItem.price, sum;
        },0);
        
        return totalPrice;
    }

}