// GLOBAL IMPORT
import {Injectable, Inject} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Subject} from "rxjs/Subject";
import 'rxjs/Rx';

@Injectable()

export class OrderService{

	private authenticate = new Subject<boolean>();
	authenticateState$ = this.authenticate.asObservable();

	constructor(@Inject(Http) private http: Http){}

    
    // place order
	orderPlace(order): Observable<any> {
		alert(order);
    	return this.http.post('/api/order', {
        	order: order
      	})
	    .map(res => res.json())
	    .catch(error => {
			return Observable.throw(error.json());
		});
	}

	// save prodcts into database
	saveCartProduct(){

	}
		
}