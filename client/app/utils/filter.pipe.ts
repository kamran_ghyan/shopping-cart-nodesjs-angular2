import { Pipe, PipeTransform } from '@angular/core';

import { Product } from '../../model/product/product';

@Pipe({name: 'filterItems'})

export class FilterItemsPipe implements PipeTransform {

    transform(values:Product[], [filterBy]) : any {
        return values.filter((item:Product)=>{
            return filterBy ? item.title.toLowerCase().includes(filterBy.toLowerCase()) : true;
        });
    }

}