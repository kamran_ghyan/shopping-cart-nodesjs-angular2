// GLOBAL IMPORT
import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs/Rx";

// SERVICES IMPORT
import { CartComponent } from '../../../components/cart/cart.component'
import { CartService } from '../../../services/cart/cart.service';
import { AuthService } from '../../../services/auth/auth.service';
import { Product } from '../../../../model/product/product';

@Component({
    moduleId: module.id,
    selector: 'header',
    templateUrl: 'header.component.html'
})

export class HeaderComponent implements OnInit{ 

    private cartItems: Product[] = [];

    isAuth: boolean;

    constructor(private cartService:CartService, private authService:AuthService){
        
        this.cartItems = cartService.getCart();
        
    }

    ngOnInit(){
     this.isAuth = this.authService.isAuthenticate();
    }
}
