"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var products_service_1 = require("../../services/products/products.service");
var cart_service_1 = require("../../services/cart/cart.service");
var ProductsComponent = (function () {
    function ProductsComponent(productService, cartService) {
        var _this = this;
        this.productService = productService;
        this.cartService = cartService;
        this.search = "";
        this.productService.getProducts()
            .subscribe(function (products) {
            _this.products = products;
        });
    }
    ProductsComponent.prototype.addToCart = function (item) {
        this.cartService.addItem(item);
        console.log(this.cartService.getTotalPrice());
    };
    return ProductsComponent;
}());
ProductsComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'products',
        templateUrl: 'products.component.html'
    }),
    __metadata("design:paramtypes", [products_service_1.ProductService, cart_service_1.CartService])
], ProductsComponent);
exports.ProductsComponent = ProductsComponent;
//# sourceMappingURL=products.component.js.map