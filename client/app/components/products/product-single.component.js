"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var products_service_1 = require("../../services/products/products.service");
var cart_service_1 = require("../../services/cart/cart.service");
var ProductSingleComponent = (function () {
    function ProductSingleComponent(productService, route, cartService) {
        var _this = this;
        this.productService = productService;
        this.route = route;
        this.cartService = cartService;
        this.cart = {};
        this.productService.getProduct(this.route.snapshot.params.id)
            .subscribe(function (product) {
            _this.product = product;
        });
    }
    ProductSingleComponent.prototype.ngOnInit = function () {
        this.product = this.productService.getProduct(this.route.snapshot.params.id);
    };
    // Add to Cart
    ProductSingleComponent.prototype.addToCart = function (item) {
        this.cartService.addItem(item);
        console.log(this.cartService.getTotalPrice());
        console.log(this.cartService.getCart());
    };
    return ProductSingleComponent;
}());
ProductSingleComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'product-single',
        templateUrl: 'product-single.component.html'
    }),
    __metadata("design:paramtypes", [products_service_1.ProductService, router_1.ActivatedRoute, cart_service_1.CartService])
], ProductSingleComponent);
exports.ProductSingleComponent = ProductSingleComponent;
//# sourceMappingURL=product-single.component.js.map