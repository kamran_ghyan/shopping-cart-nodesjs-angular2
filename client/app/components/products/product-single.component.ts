import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../../services/products/products.service';
import { CartService } from '../../services/cart/cart.service';
import { Product } from '../../../model/product/product';

@Component({
  moduleId: module.id,
  selector: 'product-single',
  templateUrl: 'product-single.component.html'
})
export class ProductSingleComponent implements OnInit { 
    
    public cart = {};

    product:  Product[];

    constructor(private productService:ProductService, private route: ActivatedRoute, private cartService:CartService){

      this.productService.getProduct(this.route.snapshot.params.id)
      .subscribe(product => {
        this.product = product;
      });

    }

    ngOnInit() {
      this.product = this.productService.getProduct(this.route.snapshot.params.id);
    }

    // Add to Cart
    addToCart(item){
      this.cartService.addItem(item);
    }
 }