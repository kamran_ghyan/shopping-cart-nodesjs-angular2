import { Component } from '@angular/core';
import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { ProductService } from '../../services/products/products.service';
import { CartService } from '../../services/cart/cart.service';
import { Product } from '../../../model/product/product';

@Component({
  moduleId: module.id,
  selector: 'products',
  templateUrl: 'products.component.html'
})
export class ProductsComponent { 
    
    products:  Product[];
    public search:string = "";

    constructor(private productService:ProductService, private cartService:CartService){
      this.productService.getProducts()
      .subscribe(products => {
        this.products = products;
      });
    }

    addToCart(item){
        this.cartService.addItem(item);
        console.log(this.cartService.getTotalPrice())
    }

 }