// IMPORT GLOBAL
import {Component} from '@angular/core';

// IMPORT SERVICE
import { CartService } from '../../services/cart/cart.service';

// IMPORT MODEL
import { Product } from '../../../model/product/product';

@Component({
    moduleId: module.id,
    selector: 'cart',
    templateUrl: 'cart.component.html'
})

export class CartComponent {

    private cartItems: Product[] = [];

    constructor(private cartService:CartService){
        this.cartItems = cartService.getCart();
    }
    
}