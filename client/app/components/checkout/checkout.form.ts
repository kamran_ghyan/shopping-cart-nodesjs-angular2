export class CheckoutForm {
  constructor(
    public billingFirstName: string,
    public billingLastName: string,
    public email: string,
    public phone: string
  ) {  }
}