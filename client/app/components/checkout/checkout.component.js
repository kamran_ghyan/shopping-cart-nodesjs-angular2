"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var CheckoutComponent = (function () {
    function CheckoutComponent() {
        // tabs active class binding
        this.billAdd = true;
        this.shipAdd = false;
        this.shipMth = false;
        this.ordOvrw = false;
        this.cnfrmOrd = false;
        // Tabs content vars binding
        this.tabBillAdd = true;
        this.tabShipAdd = false;
        this.tabShipMth = false;
        this.tabOrdOvrw = false;
        this.tabCnfrmOrd = false;
    }
    // onclick change tab
    CheckoutComponent.prototype.changeTab = function (tab) {
        if (tab == 'billAdd') {
            // Active tab title 
            this.billAdd = true;
            this.shipAdd = false;
            this.shipMth = false;
            this.ordOvrw = false;
            this.cnfrmOrd = false;
            // Active tab Content 
            this.tabBillAdd = true;
            this.tabShipAdd = false;
            this.tabShipMth = false;
            this.tabOrdOvrw = false;
            this.tabCnfrmOrd = false;
        }
        if (tab == 'shipAdd') {
            // Active tab title 
            this.billAdd = false;
            this.shipAdd = true;
            this.shipMth = false;
            this.ordOvrw = false;
            this.cnfrmOrd = false;
            // Active tab Content 
            this.tabBillAdd = false;
            this.tabShipAdd = true;
            this.tabShipMth = false;
            this.tabOrdOvrw = false;
            this.tabCnfrmOrd = false;
        }
        if (tab == 'shipMth') {
            // Active tab title 
            this.billAdd = false;
            this.shipAdd = false;
            this.shipMth = true;
            this.ordOvrw = false;
            this.cnfrmOrd = false;
            // Active tab Content 
            this.tabBillAdd = false;
            this.tabShipAdd = false;
            this.tabShipMth = true;
            this.tabOrdOvrw = false;
            this.tabCnfrmOrd = false;
        }
        if (tab == 'ordOvrw') {
            // Active tab title 
            this.billAdd = false;
            this.shipAdd = false;
            this.shipMth = false;
            this.ordOvrw = true;
            this.cnfrmOrd = false;
            // Active tab Content 
            this.tabBillAdd = false;
            this.tabShipAdd = false;
            this.tabShipMth = false;
            this.tabOrdOvrw = true;
            this.tabCnfrmOrd = false;
        }
        if (tab == 'cnfrmOrd') {
            // Active tab title 
            this.billAdd = false;
            this.shipAdd = false;
            this.shipMth = false;
            this.ordOvrw = false;
            this.cnfrmOrd = true;
            // Active tab Content 
            this.tabBillAdd = false;
            this.tabShipAdd = false;
            this.tabShipMth = false;
            this.tabOrdOvrw = false;
            this.tabCnfrmOrd = true;
        }
    };
    return CheckoutComponent;
}());
CheckoutComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'checkout',
        templateUrl: 'checkout.component.html'
    }),
    __metadata("design:paramtypes", [])
], CheckoutComponent);
exports.CheckoutComponent = CheckoutComponent;
//# sourceMappingURL=checkout.component.js.map