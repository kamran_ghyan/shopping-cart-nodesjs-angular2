// GLOBAL IMPORTS
import {Component, Output, EventEmitter} from '@angular/core';
import {Router} from '@angular/router';

// SERVICES IMPORT
import{OrderService} from '../../services/order/order.service';

@Component({
    moduleId: module.id,
    selector: 'checkout',
    templateUrl: 'checkout.component.html'
})

export class CheckoutComponent { 

      error: string;
      
      constructor(private router: Router, private orderService: OrderService){}
      
      onSubmit(form) { 
          alert();
        // Place order
		this.orderService.orderPlace(form)
			.subscribe(
				res => {
					if(res.success){
						//this.orderService.saveCartProduct();
						//this.router.navigate(['/']);

					}else{
						this.error = res.msg;
					}
				}
			)
	     }
       
       
       // tabs active class binding
       billAdd = true;
       shipAdd = false;
       shipMth = false;
       ordOvrw = false;
       cnfrmOrd = false;

       // Tabs content vars binding
       tabBillAdd = true;
       tabShipAdd = false;
       tabShipMth = false;
       tabOrdOvrw = false;
       tabCnfrmOrd = false;

        // onclick change tab
        changeTab(tab){      
            
            if(tab == 'billAdd'){
                // Active tab title 
                this.billAdd = true;
                this.shipAdd = false;
                this.shipMth = false;
                this.ordOvrw = false;
                this.cnfrmOrd = false;
                // Active tab Content 
                this.tabBillAdd = true;
                this.tabShipAdd = false;
                this.tabShipMth = false;
                this.tabOrdOvrw = false;
                this.tabCnfrmOrd = false;

            }
            if(tab == 'shipAdd'){
                // Active tab title 
                this.billAdd = false;
                this.shipAdd = true;
                this.shipMth = false;
                this.ordOvrw = false;
                this.cnfrmOrd = false;
                // Active tab Content 
                this.tabBillAdd = false;
                this.tabShipAdd = true;
                this.tabShipMth = false;
                this.tabOrdOvrw = false;
                this.tabCnfrmOrd = false;
            }
            if(tab == 'shipMth'){
                // Active tab title 
                this.billAdd = false;
                this.shipAdd = false;
                this.shipMth = true;
                this.ordOvrw = false;
                this.cnfrmOrd = false;
                // Active tab Content 
                this.tabBillAdd = false;
                this.tabShipAdd = false;
                this.tabShipMth = true;
                this.tabOrdOvrw = false;
                this.tabCnfrmOrd = false;

            }
            if(tab == 'ordOvrw'){
                // Active tab title 
                this.billAdd = false;
                this.shipAdd = false;
                this.shipMth = false;
                this.ordOvrw = true;
                this.cnfrmOrd = false;
                // Active tab Content 
                this.tabBillAdd = false;
                this.tabShipAdd = false;
                this.tabShipMth = false;
                this.tabOrdOvrw = true;
                this.tabCnfrmOrd = false;
            }
            if(tab == 'cnfrmOrd'){
                // Active tab title 
                this.billAdd = false;
                this.shipAdd = false;
                this.shipMth = false;
                this.ordOvrw = false;
                this.cnfrmOrd = true;
                // Active tab Content 
                this.tabBillAdd = false;
                this.tabShipAdd = false;
                this.tabShipMth = false;
                this.tabOrdOvrw = false;
                this.tabCnfrmOrd = true;

            }
        }

}