import { Component } from '@angular/core';
import { CategoryService } from '../../services/category/category.service';
import { Category } from '../../../model/category/category';

@Component({
  moduleId: module.id,
  selector: 'categories',
  templateUrl: 'categories.component.html'
})
export class CategoriesComponent { 
    
    categories:  Category[];

    constructor(private categoryService:CategoryService){
      this.categoryService.getCategories()
      .subscribe(categories => {
        this.categories = categories;
      });
    }

 }