import {Component} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'nav',
    templateUrl: 'nav.component.html'
})

export class NavComponent { 
    public cart = JSON.parse(localStorage.getItem('cartItems'));
}