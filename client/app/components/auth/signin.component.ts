// GLOBAL IMPORT
import {Component, Output, EventEmitter} from '@angular/core';
import {Router} from '@angular/router';

// SERVICES IMPORT
import{AuthService} from '../../services/auth/auth.service';

// MODEL IMPORT
import { SigninModel }    from '../../../model/forms/signin/signin';

@Component({
    moduleId: module.id,
	selector: 'signin',
	templateUrl: 'signin.component.html'
})

export class SignInComponent { 

	error: string;

	constructor(private router: Router, private authService: AuthService){}

	onSubmit(form){
		// Validate user
		this.authService.signin(form.email, form.password)
			.subscribe(
				res => {
					if(res.success){
						this.authService.saveToken(res.token);
						this.router.navigate(['/']);

					}else{
						this.error = res.msg;
					}
				}
			)
	}
}