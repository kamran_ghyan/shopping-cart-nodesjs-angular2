"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// GLOBAL IMPORT
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// SERVICES IMPORT
var auth_service_1 = require("../../services/auth/auth.service");
var SignUpComponent = (function () {
    function SignUpComponent(router, authService) {
        this.router = router;
        this.authService = authService;
    }
    // signin the new user if signup successfully
    SignUpComponent.prototype.signin = function (email, password) {
        var _this = this;
        this.authService.signin(email, password)
            .subscribe(function (res) {
            if (res.success) {
                _this.authService.saveToken(res.token);
                _this.router.navigate(['/']);
            }
            else {
                _this.error = res.msg;
            }
        });
    };
    // create the new user
    SignUpComponent.prototype.signup = function (form) {
        var _this = this;
        this.authService.signup(form.email, form.password)
            .subscribe(function (res) {
            if (res.success) {
                // signin the new user
                _this.signin(form.email, form.password);
            }
            else {
                _this.error = res.msg;
            }
        });
    };
    return SignUpComponent;
}());
SignUpComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'signup',
        templateUrl: 'signup.component.html'
    }),
    __metadata("design:paramtypes", [router_1.Router, auth_service_1.AuthService])
], SignUpComponent);
exports.SignUpComponent = SignUpComponent;
//# sourceMappingURL=signup.component.js.map