// Required Modules
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

// Routes for app
var index    = require('./routes/index');
var products = require('./routes/products');
var categories = require('./routes/categories');
var auth = require('./routes/auth');

// set port
var port = 3000;

// Main app
var app = express();

// View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Set static view
app.use(express.static(path.join(__dirname, 'client')))

// Body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Building Routes
app.use('/', index);
app.use('/api', products);
app.use('/api', categories);
app.use('/api', auth);

// Listing server
app.listen(port, function(){
    console.log('Server started on port ' +port);
});